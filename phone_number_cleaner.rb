require 'creek'
require 'csv'

class PhoneNumberCleaner
  def initialize
    @desktop = Dir.home + '/Desktop'
    @file = @desktop  + '/numeros.xlsx'
  end

  def call
    @valid_numbers ||= valid_numbers
  end

  def generate_csv
    puts "Total de numeros telefonicos del archivo: #{numbers.count}"
    puts "Total de numeros telefonicos correctos: #{call.count}"
    puts 'Generando CSV ...'
    CSV.open("#{@desktop}/numeros.csv", 'w') do |csv|
      valid_numbers.each do |number|
        csv << [number]
      end
    end
  end

  private

  def numbers
    doc = Creek::Book.new(@file)
    sheet = doc.sheets.first
    @numbers ||= sheet.rows.flat_map { |k, v| k.map { |row, number| number } }
  end

  def stripped_numbers
    numbers.map { |number| number.to_s.gsub(/[^0-9]/, '') }
  end

  def base_code?(numbers)
    numbers == '52' || numbers == '01'
  end

  def numbers_without_base_codes
    stripped_numbers.map { |number| number[2..-1] if base_code?(numbers) }
  end

  def valid_numbers
    stripped_numbers.select do |number|
      number.size == 10 &&
      number.squeeze.size > 1
    end
  end
end

PhoneNumberCleaner.new.generate_csv
